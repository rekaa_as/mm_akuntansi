<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\KeluarBarangModel;
use App\Model\PenerimaBarangModel;
use App\Model\BarangModel;
use App\Model\PersediaanBarangModel;
use App\Model\DetailPersediaanModel;

class KeluarBarangController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = KeluarBarangModel::all();
        return view('keluar_barang/index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = KeluarBarangModel::all();
        $penerima = PenerimaBarangModel::all();
        $barang = BarangModel::all();
        return view('keluar_barang/create',compact('data','penerima','barang'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $keluar = new KeluarBarangModel;
        $id = $request->get('id_barang');

        $keluar->id_barang = $id;
        $persediaan = PersediaanBarangModel::where('id_barang',$id)->orderBy('created_at','asc')->get();
        $keluar->id_penerima = $request->get('id_penerima');
        // dd($barang);
        $harga = $request->get('harga');
        $keluar->harga_satuan = $harga;
        
        $jml_keluar = $request->get('jumlah_keluar');
        $keluar->jumlah_keluar = $jml_keluar;
        
        $keluar->total_harga = $harga * $jml_keluar;
        $keluar->created_at = $request->get('created_at');
        $jumlahPersediaanSebelum = 0;

        // foreach($persediaan as $p){
        //     if($p->harga==$harga){
        //         $jumlahPersediaanSebelum = $p->jumlah;
        //         break;
        //     }
        // }
        // $keluar->kuantitas_sebelum = $jumlahPersediaanSebelum;
        $keluar->save();

        foreach($persediaan as $p){
            
            if($p->jumlah >= $jml_keluar){

                error_log($p->id_barang.' | '.$p->harga.' Persediaan memadai - '.'Jumlah Awal : '.$p->jumlah.' Jumlah Keluar : '.$jml_keluar);

                // update jumlah di persediaan
                $kuantitas_sebelum = $p->jumlah;
                $p->jumlah -= $jml_keluar;
                $p->save();

                // record ka tabel detail keluar barang
                $detailPersediaan = new DetailPersediaanModel;
                $detailPersediaan->id_persediaan = $p->id;
                $detailPersediaan->id_keluar = $keluar->id;
                $detailPersediaan->kuantitas_sebelum = $kuantitas_sebelum;
                $detailPersediaan->save();

                return redirect()->route('keluarbarang.index')->with('success', 'berhasil ditambahkan');
                exit();
            }else{
                error_log($p->id_barang.' | '.$p->harga.' Persediaan habis - '.'Jumlah Awal : '.$p->jumlah.' Jumlah Keluar : '.$jml_keluar);

                // kurangi jumlah keluar
                $jml_keluar -= $p->jumlah;

                // update jumlah di persediaan
                // $p->kuantitas_sebelum = 0;
                $p->jumlah = 0;
                $p->save();

                // record ka tabel keluar
                $detailPersediaan = new DetailPersediaanModel;
                $detailPersediaan->id_persediaan = $p->id;
                $detailPersediaan->id_keluar = $keluar->id;
                $detailPersediaan->kuantitas_sebelum = 0;
                $detailPersediaan->save();
            }
            
        }
        // $keluar->save();

        // $persediaan->save();

        return redirect()->route('keluarbarang.index')->with('success', 'berhasil ditambahkan');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
