<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\KeluarBarangModel;
use App\Model\PengirimanBarangModel;
use App\Model\BarangModel;
use DB;

class LaporanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $barangId = 1;
        $bulan = 5;
        $tahun = "";

        $first = KeluarBarangModel::addSelect(DB::raw("*, 'OUT' as type, jumlah_keluar as kuantitas"))
            ->with(["barang"])
            ->whereMonth('created_at', '=', $bulan)
            ->where('id_barang', $barangId);

        $laporan = PengirimanBarangModel::addSelect(DB::raw("*, 'IN' as type, jumlah_masuk as kuantitas"))
            ->with(["barang"])
            ->whereMonth('created_at', '=', $bulan)
            ->where('id_barang', $barangId)
                    ->union($first)
                    ->orderBy('created_at','asc')
                    ->get();
        $firstLast = KeluarBarangModel::addSelect(DB::raw("*, 'OUT' as type, jumlah_keluar as kuantitas"))
            ->with(["barang"])
            ->whereMonth('created_at', '=', $bulan-1)
            ->where('id_barang', $barangId);
        $laporanBulanLalu = PengirimanBarangModel::addSelect(DB::raw("*, 'IN' as type, jumlah_masuk as kuantitas"))
            ->with(["barang"])
            ->whereMonth('created_at', '=', $bulan-1)
            ->where('id_barang', $barangId)
                    ->union($firstLast)
                    ->orderBy('created_at','asc')
                ->get();
                    
        $data['lastLaporan'] = $laporanBulanLalu;
        $data['laporan'] = $laporan;

        $barang = BarangModel::all();
        return view("laporan/index", $data, compact('barang'));
    }
}
