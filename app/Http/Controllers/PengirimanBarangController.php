<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\PengirimanBarangModel;
use App\Model\BarangModel;
use App\Model\PengirimModel;
use App\Model\PersediaanBarangModel;

class PengirimanBarangController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = PengirimanBarangModel::all();
        return view('pengiriman_barang/index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = PengirimanBarangModel::all();
        $pengirim = PengirimModel::all();
        $barang = BarangModel::all();
        return view('pengiriman_barang/create',compact('data','pengirim','barang'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $masuk = new PengirimanBarangModel;
        //Masuk Log Transaksi Masuk

        $id = $request->get('id_barang');
        $masuk->id_barang = $id;
        $masuk->id_pengirim = $request->get('id_pengirim');

        $harga=$request->get('harga_satuan');
        $masuk->harga_satuan = $harga;

        $jml_masuk=$request->get('jumlah_masuk');
        $masuk->jumlah_masuk = $jml_masuk;

        $masuk->total_harga = $harga * $jml_masuk;
        $masuk->created_at = $request->get('created_at');
        $masuk->save();
        
        $persediaanBarang = PersediaanBarangModel::where('id_barang',$id)->where('harga',$harga)->first();
        if(!empty($persediaanBarang)){
                $persediaanBarang->jumlah += $jml_masuk;
                $persediaanBarang->save();
        }else{
            $persediaan = new PersediaanBarangModel;
            $persediaan->id_barang = $id;

            $persediaan->harga = $harga;
            $persediaan->jumlah = $jml_masuk;
            $persediaan->expired_date = $request->get('expired_date');
            $persediaan->created_at = $request->get('created_at');
            $persediaan->save();
        }
        
        return redirect()->route('masukbarang.index')->with('success', 'berhasil ditambahkan');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
