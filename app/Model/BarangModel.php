<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BarangModel extends Model
{
    protected $table = "tbl_barang";    

    protected $fillable = ["nama","satuan"];

    public function kategori_barang(){
        return $this->belongsTo('App\Model\KategoriBarangModel');
    }
    public function persediaan(){
        return $this->hasMany('App\Model\PersediaanBarangModel');
    }
    public function pengiriman(){
        return $this->hasMany('App\Model\PengirimanBarangModel');
    }
    public function keluar(){
        return $this->hasMany('App\Model\KeluarBarangModel');
    }
}
