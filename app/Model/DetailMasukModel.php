<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DetailMasukModel extends Model
{
    protected $table = "tbl_detail_masuk";    
    
    public function persediaan(){
        return $this->belongsTo('App\Model\PersediaanBarangModel');
    }
    public function masuk(){
        return $this->belongsTo('App\Model\PengirimanBarangModel');
    }
}
