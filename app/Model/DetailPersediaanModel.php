<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DetailPersediaanModel extends Model
{
    protected $table = "tbl_detail_persediaan";    
    
    public function persediaan(){
        return $this->belongsTo('App\Model\PersediaanBarangModel');
    }
    public function keluar(){
        return $this->belongsTo('App\Model\KeluarBarangModel');
    }
}
