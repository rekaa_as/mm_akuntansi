<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class KategoriBarangModel extends Model
{
    protected $table = "tbl_kategori_barang";    
    
    protected $fillable = ["nama"];

    public function barang(){
        return $this->hasMany('App\Model\BarangModel');
    }
}
