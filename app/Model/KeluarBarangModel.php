<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class KeluarBarangModel extends Model
{
    protected $table = "tbl_keluar_barang";    

    public $timestamps = true;

    protected $fillable = ["harga_satuan","jumlah_keluar","total_harga","expired_date"];

    public function barang(){
        return $this->belongsTo('App\Model\BarangModel', 'id_barang');
    }
    public function penerima(){
        return $this->belongsTo('App\Model\PengirimModel');
    }
    public function detailPersediaan(){
        return $this->belongsTo('App\Model\DetailPersediaanModel', 'id_keluar');
    }
}
