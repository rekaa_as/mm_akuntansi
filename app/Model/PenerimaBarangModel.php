<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PenerimaBarangModel extends Model
{
    protected $table = "tbl_penerima_barang";    

    protected $fillable = ["nama","alamat","no_telepon"];

    public function keluar(){
        return $this->hasMany('App\Model\KeluarBarangModel');
    }
}
