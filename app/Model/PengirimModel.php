<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PengirimModel extends Model
{
    protected $table = "tbl_pengirim";    

    protected $fillable = ["nama","alamat","no_telepon"];

    public function pengiriman(){
        return $this->hasMany('App\Model\PengirimanBarangModel');
    }
}
