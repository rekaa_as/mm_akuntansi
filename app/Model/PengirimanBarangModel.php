<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PengirimanBarangModel extends Model
{
    protected $table = "tbl_pengiriman_barang";    

    public $timestamps = true;

    protected $fillable = ["harga_satuan","jumlah_masuk","total_harga","expired_date"];

    public function barang(){
        return $this->belongsTo('App\Model\BarangModel', 'id_barang');
    }
    public function pengirim(){
        return $this->belongsTo('App\Model\PengirimModel');
    }
}
