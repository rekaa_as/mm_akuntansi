<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PersediaanBarangModel extends Model
{
    protected $table = "tbl_persediaan_barang";    

    public $timestamps = true;

    protected $fillable = ["harga","jumlah","expired_date","created_at"];

    public function barang(){
        return $this->belongsTo('App\Model\BarangModel');
    }

    public function detailPersediaan(){
        return $this->hasMany('App\Model\DetailPersediaanModel');
    }

    public function keluarBarang(){
        return $this->belongsToMany('App\Model\KeluarBarangModel');
    }

    public function getDate(){
        $date = $this->attributes['creted_at'];
        return $date;
    }
}
