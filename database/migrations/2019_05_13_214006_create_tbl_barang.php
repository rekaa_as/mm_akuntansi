<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblBarang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_barang', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_kategori_barang')->unsigned();
            $table->string('nama');
            $table->string('satuan');
            $table->timestamps();

            //Relation Entity
            $table->foreign('id_kategori_barang')->references('id')->on('tbl_kategori_barang')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_barang');
    }
}
