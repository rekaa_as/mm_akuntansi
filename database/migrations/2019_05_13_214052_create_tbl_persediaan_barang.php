<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblPersediaanBarang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_persediaan_barang', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_barang')->unsigned();
            $table->bigInteger('harga');
            $table->integer('jumlah');
            $table->bigInteger('expired_date');
            $table->timestamps();

            $table->foreign('id_barang')->references('id')->on('tbl_barang')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_persediaan_barang');
    }
}
