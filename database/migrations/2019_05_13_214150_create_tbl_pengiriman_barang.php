<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblPengirimanBarang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_pengiriman_barang', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_barang')->unsigned();
            $table->bigInteger('id_pengirim')->unsigned();
            $table->bigInteger('harga_satuan');
            $table->integer('jumlah_masuk');
            $table->bigInteger('total_harga');
            $table->timestamps();

            //Relation Entity
            $table->foreign('id_barang')->references('id')->on('tbl_barang')->onDelete('cascade');
            $table->foreign('id_pengirim')->references('id')->on('tbl_pengirim')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_pengiriman_barang');
    }
}
