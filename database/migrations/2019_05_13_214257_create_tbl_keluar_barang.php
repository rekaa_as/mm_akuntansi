<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblKeluarBarang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_keluar_barang', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_barang')->unsigned();
            $table->bigInteger('id_penerima')->unsigned();
            $table->bigInteger('harga_satuan');
            $table->integer('jumlah_keluar');
            $table->bigInteger('total_harga');
            $table->timestamps();

            $table->foreign('id_barang')->references('id')->on('tbl_barang')->onDelete('cascade');
            $table->foreign('id_penerima')->references('id')->on('tbl_penerima_barang')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_keluar_barang');
    }
}
