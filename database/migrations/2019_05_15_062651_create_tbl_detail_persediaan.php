<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblDetailPersediaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_detail_persediaan', function (Blueprint $table) {
            $table->bigInteger('id_persediaan')->unsigned();
            $table->bigInteger('id_keluar')->unsigned();
            $table->timestamps();

            $table->foreign('id_persediaan')->references('id')->on('tbl_persediaan_barang')->onDelete('cascade');
            $table->foreign('id_keluar')->references('id')->on('tbl_keluar_barang')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_detail_keluar');
    }
}
