<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblDetailMasuk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_detail_masuk', function (Blueprint $table) {
            $table->bigInteger('id_persediaan')->unsigned();
            $table->bigInteger('id_masuk')->unsigned();

            $table->foreign('id_persediaan')->references('id')->on('tbl_persediaan_barang')->onDelete('cascade');
            $table->foreign('id_masuk')->references('id')->on('tbl_pengiriman_barang')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_detail_masuk');
    }
}
