@extends('layouts.app')

@section('content')
<div class="container">
  <h2>Tambah Barang</h2><br/>
  <form method="post" action="{{ action('BarangController@store') }}" enctype="multipart/form-data">
    @csrf
    <div class="row">
      <div class="col-md-12"></div>
      <div class="form-group col-md-4">
        <label for="nama">Kategori Barang :</label>
        <select type="text" name="id_kategori_barang" class="form-control">
          @foreach($data as $d)
          <option value="{{ $d->id }}">{{ $d->nama }}</option>
          @endforeach
        </select>
      </div>
      <div class="form-group col-md-4">
        <label for="nama">Nama Barang :</label>
        <input type="text" class="form-control" name="nama">
      </div>
      <div class="form-group col-md-4">
        <label for="nama">Satuan Barang :</label>
        <input type="text" class="form-control" name="satuan">
      </div>
    </div>

    <div class="row">
      <div class="col-md-12"></div>
      <div class="form-group col-md-4" style="margin-top:10px">
        <button type="submit" class="btn btn-success">Tambah</button>
      </div>
    </div>
  </form>
</div>
@endsection