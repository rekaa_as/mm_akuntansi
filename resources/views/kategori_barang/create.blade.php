@extends('layouts.app')

@section('content')
<div class="container">
  <h2>Tambah Kategori</h2><br/>
  <form method="post" action="{{ action('KategoriBarangController@index') }}" enctype="multipart/form-data">
    @csrf
    <div class="row">
      <div class="col-md-12"></div>
      <div class="form-group col-md-4">
        <label for="nama">Nama Kategori Barang :</label>
        <input type="text" class="form-control" name="nama">
      </div>
    </div>

    <div class="row">
      <div class="col-md-12"></div>
      <div class="form-group col-md-4" style="margin-top:10px">
        <button type="submit" class="btn btn-success">Tambah</button>
      </div>
    </div>
  </form>
</div>
@endsection