@extends('layouts.app')

@section('content')
<div class="container">
    <a href="{{ action('KategoriBarangController@create') }}">Tambah Kategori Barang</a>
    <table class="table table-striped table-bordered">
      <thead>
        <tr>
          <th>No</th>
          <th>Nama Kategori</th>
          <!-- <th colspan="2">Action</th> -->
        </tr>
      </thead>
      <tbody>
        
        <?php $no=1 ?>
        
        @foreach($data as $d)
          <tr>
            <td>{{ $no++ }}</td>
            <td>{{ $d->nama }}</td>

            <!-- <td>
                <form action="{{ action('BarangController@destroy', $d->id) }}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <a href="{{ action('BarangController@edit', $d->id) }}" class=" btn btn-sm btn-primary">Edit</a>
                    <button class="btn btn-sm btn-danger" type="submit" onclick="return confirm('Yakin ingin menghapus data?')">Delete</button>
                </form>
            </td> -->
          </tr>
        @endforeach
      </tbody>
    </table>
</div>
@endsection