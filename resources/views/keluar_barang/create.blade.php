@extends('layouts.app')

@section('content')
<div class="container">
  <h2>Barang Keluar</h2><br/>
  <form method="post" action="{{ action('KeluarBarangController@store') }}" enctype="multipart/form-data">
    @csrf
    <div class="row">
      <div class="col-md-12"></div>
      <div class="form-group col-md-4">
        <label for="nama">Penerima Barang:</label>
        <select type="text" name="id_penerima" class="form-control">
          @foreach($penerima as $p)
          <option value="{{ $p->id }}">{{ $p->nama }}</option>
          @endforeach
        </select>
      </div>
      <div class="form-group col-md-4">
        <label for="nama">Barang :</label>
        <select type="text" name="id_barang" class="form-control">
          @foreach($barang as $b)
          <option value="{{ $b->id }}">{{ $b->nama }}</option>
          @endforeach
        </select>
      </div>
      <div class="form-group col-md-4">
        <label for="nama">Harga keluar :</label>
        <input type="text" class="form-control" name="harga">
      </div>
      <div class="form-group col-md-4">
        <label for="nama">Jumlah keluar :</label>
        <input type="text" class="form-control" name="jumlah_keluar">
      </div>
      <div class="form-group col-md-4">
        <label for="nama">Tanggal Keluar :</label>
        <input type="date" class="form-control" name="created_at">
      </div>
    </div>

    <div class="row">
      <div class="col-md-12"></div>
      <div class="form-group col-md-4" style="margin-top:10px">
        <button type="submit" class="btn btn-success">Tambah</button>
      </div>
    </div>
  </form>
</div>
@endsection