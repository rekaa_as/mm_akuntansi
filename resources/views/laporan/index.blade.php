@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="form-group col-md-3">
      <label for="nama">Barang:</label>
      <select type="text" name="id_barang" class="form-control">
        <option value="">Pilih Barang</option>
        @foreach($barang as $b)
          <option value="{{$b->id}}">{{$b->nama}}</option>
        @endforeach
      </select>
    </div>
    <div class="form-group col-md-3">
      <label for="nama">Bulan:</label>
      <select type="text" name="bulan" class="form-control">
        <option value="">Pilih Bulan</option>
        <option value="1">Januari</option>
        <option value="2">Februari</option>
        <option value="3">Maret</option>
        <option value="4">April</option>
        <option value="5">Mei</option>
        <option value="6">Juni</option>
        <option value="7">Juli</option>
        <option value="8">Agustus</option>
        <option value="9">September</option>
        <option value="10">Oktober</option>
        <option value="11">November</option>
        <option value="12">Desember</option>
      </select>
    </div>
    <div class="form-group col-md-3">
      <label for="nama">Tahun:</label>
      <select type="text" name="id_penerima" class="form-control">
        <option value="">Pilih Tahun</option>
        @for($year=1900; $year <= now()->year; $year++)
          <option value="{{$year}}">{{$year}}</option>
        @endfor
      </select>
    </div>
  </div>
    <table class="table table-striped table-bordered">
      <thead>
        <tr>
            <th rowspan="4">Tanggal</th>
        </tr> 
        <tr>
            <th colspan="3" rowspan="2">Saldo Awal</th>
            <th colspan="6">Mutasi</th>
            <th colspan="3" rowspan="2">Saldo Akhir</th>
        </tr>
        <tr>
            <td colspan="3">Barang Masuk</td>
            <td colspan="3">Barang Keluar</td>
        </tr>
        <tr>
            <!-- Saldo Awal -->
            <td>Harga</td>
            <td>Kuantitas</td>
            <td>Total</td>

            <!-- Mutasi Barang Masuk -->
            <td>Harga</td>
            <td>Kuantitas</td>
            <td>Total</td>

            <!-- Mutasi Barang Keluar -->
            <td>Harga</td>
            <td>Kuantitas</td>
            <td>Total</td>

            <!-- Saldo Akhir -->
            <td>Harga</td>
            <td>Kuantitas</td>
            <td>Total</td>
        </tr>
      </thead>
      <tbody>
        <?php $no=1 ?>
        <tr>
            <td>1 {{$laporan[0]->created_at->format("M Y")}}</td>
            <td>
            @foreach($lastLaporan as $llap)
              @if($llap->type == "IN")
                <li>{{$llap->harga_satuan}}</li>
              @else

              @endif
            @endforeach
            </td>
            <td>
            @foreach($lastLaporan as $llap)
              @if($llap->type == "IN")
                <li>{{$llap->kuantitas}}</li>
              @else

              @endif
            @endforeach
            </td>
            <td>
            @foreach($lastLaporan as $llap)
              @if($llap->type == "IN")
                <li>{{($llap->total_harga)}}</li>
              @else

              @endif
            @endforeach
            </td>
            <td colspan=6>

            </td>
            <td>
            @foreach($lastLaporan as $llap)
              @if($llap->type == "IN")
                <li>{{$llap->harga_satuan}}</li>
              @else

              @endif
            @endforeach
            </td>
            <td>
            @foreach($lastLaporan as $llap)
              @if($llap->type == "IN")
                <li>{{$llap->kuantitas}}</li>
              @else

              @endif
            @endforeach
            </td>
            <td>
            @foreach($lastLaporan as $llap)
              @if($llap->type == "IN")
                <li>{{($llap->total_harga)}}</li>
              @else

              @endif
            @endforeach
            </td>
        </tr>
        @foreach($laporan as $l)
        <tr>
            <td>{{$l->created_at->format("d M Y")}}</td>
            <td>A</td>
            <td>A</td>
            <td>A</td>
            @if($l->type == "IN")
            <td>{{$l->harga_satuan}}</td>
            <td>{{$l->kuantitas}}</td>
            <td>{{$l->total_harga}}</td>
            @else
            <td>0</td>
            <td>0</td>
            <td>0</td>
            @endif

            @if($l->type == "OUT")
            <td>{{$l->harga_satuan}}</td>
            <td>{{$l->kuantitas}}</td>
            <td>{{$l->total_harga}}</td>
            @else
            <td>0</td>
            <td>0</td>
            <td>0</td>
            @endif

            <!-- Saldo akhir -->
            @if($l->type == "OUT")
            <td>
            @foreach(\App\Model\DetailPersediaanModel::where('id_keluar', $l->id)->get() as $detailPersediaan)
              @php
              $persediaan = \App\Model\PersediaanBarangModel::find($detailPersediaan->id_persediaan);
              @endphp
              
              <li>{{$persediaan->harga}}</li>
  
            @endforeach
            </td>
            <td>
            @foreach(\App\Model\DetailPersediaanModel::where('id_keluar', $l->id)->get() as $detailPersediaan)
              @php
              $persediaan = \App\Model\PersediaanBarangModel::find($detailPersediaan->id_persediaan);
              @endphp
  
                <li>{{$persediaan->jumlah}}</li>
      
            @endforeach
            </td>
            <td>
            @foreach(\App\Model\DetailPersediaanModel::where('id_keluar', $l->id)->get() as $detailPersediaan)
              @php
              $persediaan = \App\Model\PersediaanBarangModel::find($detailPersediaan->id_persediaan);
              @endphp
 
                <li>{{($persediaan->harga*$persediaan->jumlah)}}</li>
        
            @endforeach
            </td>
            @elseif($l->type == "IN")
            <td>A</td>
            <td>A</td>
            <td>A</td>
            @endif
        </tr>
        @endforeach
      </tbody>
    </table>
</div>
@endsection