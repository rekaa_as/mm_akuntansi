@extends('layouts.app')

@section('content')
<div class="container">
  <h2>Barang Masuk</h2><br/>
  <form method="post" action="{{ action('PengirimanBarangController@store') }}" enctype="multipart/form-data">
    @csrf
    <div class="row">
      <div class="col-md-12"></div>
      <div class="form-group col-md-4">
        <label for="nama">Pengirim Barang:</label>
        <select type="text" name="id_pengirim" class="form-control">
          @foreach($pengirim as $supplier)
          <option value="{{ $supplier->id }}">{{ $supplier->nama }}</option>
          @endforeach
        </select>
      </div>
      <div class="form-group col-md-4">
        <label for="nama">Barang :</label>
        <select type="text" name="id_barang" class="form-control">
          @foreach($barang as $b)
          <option value="{{ $b->id }}">{{ $b->nama }}</option>
          @endforeach
        </select>
      </div>
      <div class="form-group col-md-4">
        <label for="nama">Harga Satuan :</label>
        <input type="text" class="form-control" name="harga_satuan">
      </div>
      <div class="form-group col-md-4">
        <label for="nama">Jumlah Masuk :</label>
        <input type="text" class="form-control" name="jumlah_masuk">
      </div>
      <div class="form-group col-md-4">
        <label for="nama">Tanggal Masuk :</label>
        <input type="date" class="form-control" name="created_at">
      </div>
      <div class="form-group col-md-4">
        <label for="nama">Kadaluarsa :</label>
        <input type="date" class="form-control" name="expired_date">
      </div>
    </div>

    <div class="row">
      <div class="col-md-12"></div>
      <div class="form-group col-md-4" style="margin-top:10px">
        <button type="submit" class="btn btn-success">Tambah</button>
      </div>
    </div>
  </form>
</div>
@endsection