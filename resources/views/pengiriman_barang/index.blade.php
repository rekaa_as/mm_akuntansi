@extends('layouts.app')

@section('content')
<div class="container">
    <a href="{{ action('PengirimanBarangController@create') }}">Tambah Barang Masuk</a>
    <table class="table table-striped table-bordered">
      <thead>
        <tr>
          <th>No</th>
          <th>Tanggal</th>
          <th>Barang</th>
          <th>Pengirim</th>
          <th>Harga Satuan</th>
          <th>Jumlah Masuk</th>
          <th>Total Harga</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        
        <?php $no=1 ?>
        
        @foreach($data as $d)
          <tr>
            <td>{{ $no++ }}</td>
            <td>{{ $d->created_at }}</td>
            <td>{{ $d->id_barang }}</td>
            <td>{{ $d->id_pengirim }}</td>
            <td>{{ $d->harga_satuan }}</td>
            <td>{{ $d->jumlah_masuk }}</td>
            <td>{{ $d->total_harga }}</td>

            <td>
                <form action="{{ action('PengirimanBarangController@destroy', $d->id) }}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <a href="{{ action('PengirimanBarangController@edit', $d->id) }}" class=" btn btn-sm btn-primary">Edit</a>
                    <button class="btn btn-sm btn-danger" type="submit" onclick="return confirm('Yakin ingin menghapus data?')">Delete</button>
                </form>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
</div>
@endsection