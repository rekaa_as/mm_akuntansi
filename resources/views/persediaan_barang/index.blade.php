@extends('layouts.app')

@section('content')
<div class="container">
    <!-- <a href="{{ action('PenerimaBarangController@create') }}">Tambah Data Pengirim</a> -->
    <table class="table table-striped table-bordered">
      <thead>
        <tr>
          <th>No</th>
          <th>Tanggal</th>
          <th>ID Barang</th>
          <th>Harga Barang</th>
          <th>Jumlah Barang</th>
          <th>Kadaluarsa</th>
          <!-- <th colspan="2">Action</th> -->
        </tr>
      </thead>
      <tbody>
        
        <?php $no=1 ?>
        
        @foreach($data as $d)
          <tr>
            <td>{{ $no++ }}</td>
            <td>{{ $d->created_at->format('d-m-Y') }}</td>
            <td>{{ $d->id_barang }}</td>
            <td>{{ $d->harga }}</td>
            <td>{{ $d->jumlah }}</td>
            <td>{{ $d->expired_date }}</td>

            <!-- <td>
                <form action="{{ action('PengirimController@destroy', $d->id) }}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <a href="{{ action('PengirimController@edit', $d->id) }}" class=" btn btn-sm btn-primary">Edit</a>
                    <button class="btn btn-sm btn-danger" type="submit" onclick="return confirm('Yakin ingin menghapus data?')">Delete</button>
                </form>
            </td> -->
          </tr>
        @endforeach
      </tbody>
    </table>
</div>
@endsection