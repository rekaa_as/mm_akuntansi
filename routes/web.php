<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', 'DashboardController@index');
Route::get('/laporan', 'LaporanController@index');

Route::resource('/kategoribarang','KategoriBarangController');
Route::resource('/pengirim','PengirimController');
Route::resource('/penerima','PenerimaBarangController');
Route::resource('/masukbarang','PengirimanBarangController');
Route::resource('/keluarbarang','KeluarBarangController');
Route::resource('/persediaan','PersediaanBarangController');
Route::resource('/barang','BarangController');